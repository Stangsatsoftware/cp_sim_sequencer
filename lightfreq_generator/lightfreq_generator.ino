#include "IOShieldOled.h"

#define SLEEP 4200
#define WAKE 1300
int powhigh = 0;
int elapse = 0;
const int PWM_PIN = 3;
int loopcount = 0;
int loopcount2 = 0;
char outputstring[50];
char buffstring[10];
int i = 0;

void setup() {
    pinMode(9, OUTPUT);
    IOShieldOled.begin();
    analogWriteResolution(10);
    digitalWrite(9,LOW);
}

void loop() {
  
  loopcount2 = 0;
  
  while(digitalRead(35) == LOW)
{
      manualmode();
    }
    
  loopcount2 = 0;
  
 while(digitalRead(35) == HIGH)
{
       sequencermode();
    }
    
   loopcount2 = 0;
   
 
}
 

  
void sequencermode()
{
  if (loopcount2 == 0)
  {
  IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("SEQUENCER");
      IOShieldOled.setCursor(0, 1);
      IOShieldOled.putString("1:ABORT 2:SLEEP");
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString("3:FULL 4:AUTO");


      loopcount2++;
  }


if(digitalRead(37) == HIGH){
       abortseq();
  }
if(digitalRead(36) == HIGH){
       sleepseq();
  }
if(digitalRead(34) == HIGH){
       fullseq();
  }
if(digitalRead(4) == HIGH){
  flashlightseq();
  delay(5000);
  abortseq();
  delay(5000);
  fullseq();
  delay(5000);
  sleepseq();
  delay(5000);
  
  }


      
   if(digitalRead(4) == HIGH){
       analogWriteFrequency(WAKE);
       analogWrite(PWM_PIN, 512);
       for(i=0;i<21;i++)
       {
        strcpy(outputstring, "Wake for ");
        sprintf(buffstring, "%d", 20-i);

        strcat(outputstring, buffstring);
         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString(outputstring);
            sprintf(buffstring, "%d", elapse);

      IOShieldOled.setCursor(0, 1);
      IOShieldOled.putString("Elapsed Time: ");
      IOShieldOled.putString(buffstring);
      delay(1000);
            elapse++;

       } 
             
       analogWrite(PWM_PIN, LOW);
 for(i=0;i<31;i++)
       {
        sprintf(buffstring, "%d", 30-i);
        strcpy(outputstring,buffstring);
        strcat(outputstring, " to Sleep");
         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString(outputstring);
      sprintf(buffstring, "%d", elapse);

      IOShieldOled.setCursor(0, 1);
      IOShieldOled.putString("Elapsed Time: ");
      IOShieldOled.putString(buffstring);


      delay(1000);
      elapse++;
       } 

       
       analogWriteFrequency(SLEEP);
       analogWrite(PWM_PIN, 512);
              for(i=0;i<7;i++)
       {
        strcpy(outputstring, "Sleep for ");
        sprintf(buffstring, "%d", 6-i);

        strcat(outputstring, buffstring);

         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString(outputstring);
            sprintf(buffstring, "%d", elapse);

      IOShieldOled.setCursor(0, 1);
      IOShieldOled.putString("Elapsed Time: ");
      IOShieldOled.putString(buffstring);
      delay(1000);
      elapse++;
       } 
      
      analogWrite(PWM_PIN, LOW);
            loopcount2 = 0;
  }
}

void manualmode()
{
  if (loopcount2 == 0){
      IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("MANUAL");
      IOShieldOled.setCursor(0, 1);
      IOShieldOled.putString("S1:MANUAL/SEQUEN");
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString("S4:SAT POWER");
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString("B1:W B2:S B3:F");
      loopcount2++;
    }
  loopcount = 0;

  analogWrite(PWM_PIN, LOW);
  
  if(digitalRead(2) == HIGH)
  {
    if (powhigh == 0){
    powhigh = 1;
    digitalWrite(9, HIGH);

  }
  }
  else if(powhigh == 1)
  {
    powhigh = 0;
        digitalWrite(9, LOW);

  }


  while (digitalRead(37) == HIGH) {
    
       if (loopcount == 0){
       analogWriteFrequency(WAKE);
       analogWrite(PWM_PIN, 512);
       IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("WAKE WAKE WAKE");
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString("1.3 kHz");
      

       loopcount++;
       }
       loopcount2=0;

  }

  loopcount = 0;
    while (digitalRead(36) == HIGH) {
      if(loopcount == 0){
       analogWriteFrequency(SLEEP);
       analogWrite(PWM_PIN, 512);
      IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("SLEEP SLEEP");
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString("4.2 kHz");
       loopcount++;
      }
       loopcount2=0;

  }
  while (digitalRead(34) == HIGH) {
       if (loopcount == 0){
       //analogWriteFrequency(WAKE);
       digitalWrite(3,HIGH);
             IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("NOISE NOISE");
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString("No Frequency");

       loopcount++;
       }
       loopcount2=0;

  }
  }

void timemode()
{
   if (loopcount2 == 0){
          IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("You are in:");
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString("TIMED MODE");
      loopcount2++;
  }
  analogWrite(PWM_PIN, LOW);
  


  while (digitalRead(37) == HIGH) {
       if (loopcount == 0){
       analogWriteFrequency(WAKE);
       analogWrite(PWM_PIN, 512);
       loopcount++;
       for(i=0;i<19;i++)
       {
        strcpy(outputstring, "Wake for ");
        sprintf(buffstring, "%d", 20-i);

        strcat(outputstring, buffstring);
         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
       
       }
              loopcount2=0;


       
       analogWrite(PWM_PIN, LOW);

       

  }
  loopcount = 0;
    while (digitalRead(36) == HIGH) {
      if(loopcount == 0){
       analogWriteFrequency(SLEEP);
       analogWrite(PWM_PIN, 512);
              for(i=0;i<5;i++)
       {
        strcpy(outputstring, "Sleep for ");
        sprintf(buffstring, "%d", 6-i);

        strcat(outputstring, buffstring);

         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
       loopcount++;
      }
      analogWrite(PWM_PIN, LOW);

  }
    while (digitalRead(34) == HIGH) {
       if (loopcount == 0){
       //analogWriteFrequency(WAKE);
       digitalWrite(3,HIGH);
       loopcount++;
       }

  }
}

void abortseq(){
  analogWriteFrequency(WAKE);
  IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Applying Power");
      digitalWrite(9,HIGH);
      delay(1000);
      
       analogWrite(PWM_PIN, 512);
       for(i=0;i<20;i++)
       {
        strcpy(outputstring, "Wake for ");
        sprintf(buffstring, "%d", 20-i);

        strcat(outputstring, buffstring);
         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Abort Sequence");

      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
             
       analogWrite(PWM_PIN, LOW);
 for(i=0;i<23;i++)
       {
        sprintf(buffstring, "%d", 23-i);
        strcpy(outputstring,buffstring);
        strcat(outputstring, " to Sleep");
        // IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 

       
       analogWriteFrequency(SLEEP);
       analogWrite(PWM_PIN, 512);
              for(i=0;i<6;i++)
       {
        strcpy(outputstring, "Sleep for ");
        sprintf(buffstring, "%d", 6-i);

        strcat(outputstring, buffstring);

        // IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
      analogWrite(PWM_PIN, LOW);
        delay(9000);
      loopcount2 = 0;
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Disconnecting Power");
      digitalWrite(9,LOW);
      delay(1000);
}

void fullseq(){
  analogWriteFrequency(WAKE);
  IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Applying Power");
      digitalWrite(9,HIGH);
      delay(1000);
      
       analogWrite(PWM_PIN, 512);
       for(i=0;i<20;i++)
       {
        strcpy(outputstring, "Wake for ");
        sprintf(buffstring, "%d", 20-i);

        strcat(outputstring, buffstring);
         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("False EOM VER");

      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
             
       analogWrite(PWM_PIN, LOW);
 for(i=0;i<1810;i++)
       {
        sprintf(buffstring, "%d", 1810-i);
        strcpy(outputstring,buffstring);
        strcat(outputstring, " to Sleep");
        // IOShieldOled.clearBuffer();
        IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("False EOM VER");
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 

       
       analogWriteFrequency(SLEEP);
       analogWrite(PWM_PIN, 512);
              for(i=0;i<6;i++)
       {
        strcpy(outputstring, "Sleep for ");
        sprintf(buffstring, "%d", 6-i);

        strcat(outputstring, buffstring);

        // IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       }
       analogWrite(PWM_PIN, 512);


         for(i=0;i<864;i++)
       {
        sprintf(buffstring, "%d", 864-i);
        strcpy(outputstring,buffstring);
        strcat(outputstring, " to LAS Reset");
        // IOShieldOled.clearBuffer();
        IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("False EOM VER");
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
      
      analogWrite(PWM_PIN, LOW);
      loopcount2 = 0;
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Disconnecting Power");
      digitalWrite(9,LOW);
      delay(1000);
}

void sleepseq(){
  analogWriteFrequency(WAKE);
  IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Applying Power");
      digitalWrite(9,HIGH);
      delay(1000);
      
       analogWrite(PWM_PIN, 512);
       for(i=0;i<20;i++)
       {
        strcpy(outputstring, "Wake for ");
        sprintf(buffstring, "%d", 20-i);

        strcat(outputstring, buffstring);
         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Sleep Sequence");

      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 
             
       analogWrite(PWM_PIN, LOW);
 for(i=0;i<32;i++)
       {
        sprintf(buffstring, "%d", 32-i);
        strcpy(outputstring,buffstring);
        strcat(outputstring, " to Sleep");
        // IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 

       
       analogWriteFrequency(SLEEP);
       analogWrite(PWM_PIN, 512);
              for(i=0;i<6;i++)
       {
        strcpy(outputstring, "Sleep for ");
        sprintf(buffstring, "%d", 6-i);

        strcat(outputstring, buffstring);

        // IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString(outputstring);
      delay(1000);
       }
         analogWrite(PWM_PIN, LOW); 

                     for(i=0;i<30;i++)
       {
        strcpy(outputstring, "Time Remaining: ");
        sprintf(buffstring, "%d", 30-i);

        strcat(outputstring, buffstring);

         IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Lift Cube"); 
       
      IOShieldOled.setCursor(0, 2);
      IOShieldOled.putString(outputstring);
      delay(1000);
       } 

      loopcount2 = 0;
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Disconnecting Power");
            delay(1000);
      digitalWrite(9,LOW);

}

void flashlightseq(){

  IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Flashlight Test");
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString("Applying Power");
      digitalWrite(9,HIGH);
      delay(1000);
       analogWriteFrequency(3000);

       analogWrite(3, 1023);
      IOShieldOled.clearBuffer();
      IOShieldOled.setCursor(0, 0);
      IOShieldOled.putString("Flashlight Test");
      IOShieldOled.setCursor(0, 3);

      IOShieldOled.putString("Flash for 1");
      delay(1000);
       
       analogWrite(3, LOW);

       for(i=0;i<28;i++)
       {
        
        strcpy(outputstring, "Power Off in ");
        sprintf(buffstring, "%d", 28-i);
        strcat(outputstring, buffstring);
        IOShieldOled.clearBuffer();
        IOShieldOled.setCursor(0, 0);
        IOShieldOled.putString("Flashlight Test");
        IOShieldOled.setCursor(0, 3);

        IOShieldOled.putString(outputstring);

        // IOShieldOled.clearBuffer();
        delay(1000);
       } 
      
      
      loopcount2 = 0;
      IOShieldOled.setCursor(0, 3);
      IOShieldOled.putString("Disconnecting Power");
      digitalWrite(9,LOW);
      delay(1000);
}
